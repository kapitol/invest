<? include 'invest/header.php'?>
<div class="container">
  <div class="row mb-4 ">
    <div class="col-md-12 col-lg-4">
      <div class="card h-100 border-0 mb-4 mb-lg-0">
        <div class="card-body p-0">
          <div class="agent-contract">
            <h2 class="mb-4 d-flex align-items-center">Агентский договор №143</h2>
            <h3 class="mb-2">Сретификат партнера: <span>есть</span></h3>
            <div class="agent-contract__status mb-2">
              Представитель:&nbsp;<span>нет</span> 
            </div>
            <a class="mb-3 d-block agent-contract__link font-weight-light" href="#">(как стать представителем?)</a>
            <a href="#" class="agent-contract-btn-onlain btn btn-lg btn-primary btn-block">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" xml:space="preserve" width="15px" height="15px">
                <path d="M50.688,48.222C55.232,43.101,58,36.369,58,29c0-7.667-2.996-14.643-7.872-19.834c0,0,0-0.001,0-0.001  c-0.004-0.006-0.01-0.008-0.013-0.013c-5.079-5.399-12.195-8.855-20.11-9.126l-0.001-0.001L29.439,0.01C29.293,0.005,29.147,0,29,0  s-0.293,0.005-0.439,0.01l-0.563,0.015l-0.001,0.001c-7.915,0.271-15.031,3.727-20.11,9.126c-0.004,0.005-0.01,0.007-0.013,0.013  c0,0,0,0.001-0.001,0.002C2.996,14.357,0,21.333,0,29c0,7.369,2.768,14.101,7.312,19.222c0.006,0.009,0.006,0.019,0.013,0.028  c0.018,0.025,0.044,0.037,0.063,0.06c5.106,5.708,12.432,9.385,20.608,9.665l0.001,0.001l0.563,0.015C28.707,57.995,28.853,58,29,58  s0.293-0.005,0.439-0.01l0.563-0.015l0.001-0.001c8.185-0.281,15.519-3.965,20.625-9.685c0.013-0.017,0.034-0.022,0.046-0.04  C50.682,48.241,50.682,48.231,50.688,48.222z M2.025,30h12.003c0.113,4.239,0.941,8.358,2.415,12.217  c-2.844,1.029-5.563,2.409-8.111,4.131C4.585,41.891,2.253,36.21,2.025,30z M8.878,11.023c2.488,1.618,5.137,2.914,7.9,3.882  C15.086,19.012,14.15,23.44,14.028,28H2.025C2.264,21.493,4.812,15.568,8.878,11.023z M55.975,28H43.972  c-0.122-4.56-1.058-8.988-2.75-13.095c2.763-0.968,5.412-2.264,7.9-3.882C53.188,15.568,55.736,21.493,55.975,28z M28,14.963  c-2.891-0.082-5.729-0.513-8.471-1.283C21.556,9.522,24.418,5.769,28,2.644V14.963z M28,16.963V28H16.028  c0.123-4.348,1.035-8.565,2.666-12.475C21.7,16.396,24.821,16.878,28,16.963z M30,16.963c3.179-0.085,6.3-0.566,9.307-1.438  c1.631,3.91,2.543,8.127,2.666,12.475H30V16.963z M30,14.963V2.644c3.582,3.125,6.444,6.878,8.471,11.036  C35.729,14.45,32.891,14.881,30,14.963z M40.409,13.072c-1.921-4.025-4.587-7.692-7.888-10.835  c5.856,0.766,11.125,3.414,15.183,7.318C45.4,11.017,42.956,12.193,40.409,13.072z M17.591,13.072  c-2.547-0.879-4.991-2.055-7.294-3.517c4.057-3.904,9.327-6.552,15.183-7.318C22.178,5.38,19.512,9.047,17.591,13.072z M16.028,30  H28v10.038c-3.307,0.088-6.547,0.604-9.661,1.541C16.932,37.924,16.141,34.019,16.028,30z M28,42.038v13.318  c-3.834-3.345-6.84-7.409-8.884-11.917C21.983,42.594,24.961,42.124,28,42.038z M30,55.356V42.038  c3.039,0.085,6.017,0.556,8.884,1.4C36.84,47.947,33.834,52.011,30,55.356z M30,40.038V30h11.972  c-0.113,4.019-0.904,7.924-2.312,11.58C36.547,40.642,33.307,40.126,30,40.038z M43.972,30h12.003  c-0.228,6.21-2.559,11.891-6.307,16.348c-2.548-1.722-5.267-3.102-8.111-4.131C43.032,38.358,43.859,34.239,43.972,30z   M9.691,47.846c2.366-1.572,4.885-2.836,7.517-3.781c1.945,4.36,4.737,8.333,8.271,11.698C19.328,54.958,13.823,52.078,9.691,47.846  z M32.521,55.763c3.534-3.364,6.326-7.337,8.271-11.698c2.632,0.945,5.15,2.209,7.517,3.781  C44.177,52.078,38.672,54.958,32.521,55.763z" fill="#FFFFFF"/>
              </svg>
              Оформить заявку
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4">
      <div class="card d-flex flex-column justify-content-between h-100 mb-4 mb-lg-0 border-md-0">
        <div class="card-body cooperators d-flex flex-column justify-content-between text-center bg-secondary">
          <div>
            <h3 class="mt-2 mb-5">Сотрудиники</h3>
            <ul class="list-unstyled text-left">
              <li><a href="#">Сидоров Кирилл Петрович</a></li>
              <li><a href="#">Козлов Василий Сергеевич</a></li>
              <li><a href="#">Бирюков Савелий Дмитриевич</a></li>
            </ul>
          </div>
          <div>
            <button class="btn btn-primary btn-block btn-lg cooperators-btn" data-toggle="modal" data-target="#brokerEnding">
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="15px" height="15px" viewBox="0 0 37.644 37.644" style="enable-background:new 0 0 37.644 37.644;"
                xml:space="preserve">
                <path d="M37.644,5.957c0,0.829-0.672,1.5-1.5,1.5h-2.118v2.119c0,0.829-0.672,1.5-1.5,1.5s-1.5-0.671-1.5-1.5V7.457h-2.119
                  c-0.828,0-1.5-0.671-1.5-1.5c0-0.829,0.672-1.5,1.5-1.5h2.119V2.339c0-0.829,0.672-1.5,1.5-1.5s1.5,0.671,1.5,1.5v2.118h2.118
                  C36.972,4.457,37.644,5.129,37.644,5.957z M33.246,28.093l-10.557-3.299c2.28-2.717,3.668-7.024,3.668-11.055
                  C26.357,7.64,22.88,3.7,17.502,3.7c-5.38,0-8.857,3.94-8.857,10.039c0,4.03,1.387,8.338,3.669,11.055L1.756,28.093
                  C0.711,28.419,0,29.384,0,30.48v3.824c0,1.383,1.119,2.5,2.5,2.5h30.002c1.381,0,2.5-1.117,2.5-2.5V30.48
                  C35.002,29.386,34.291,28.42,33.246,28.093z"/>
                </svg>
              Добавить
            </button> 
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4">
      <div class="card border-0 h-100 bg-secondary mt-4 mt-lg-0">
        <div class="card-body personal d-flex flex-column justify-content-between m-auto w-100">
          <div>
            <h3 class="text-center mb-4 mt-2">
              ООО Финброкер24
            </h3>
            <div class="personal-data">
              <a href="tel:+79123456789" class="personal-data__item d-block">
                <span>Телефон:</span> +7 (912) 345-67-89
              </a>
              <a href="mailto:ivanov.ag@yandex.ru" class="personal-data__item d-block">
                <span>E-mail:</span> ivanov.ag@yandex.ru
              </a>
            </div>
          </div>
          <div class="card-line my-4"></div>
          <div>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-user"></i>Профиль</button>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-pencil-alt"></i>редактировать</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-5 mb-5 mt-lg-0">
    <div class="col-lg-8 col-md-12">
    
      <div class="broker-block">
        <div class="broker-block__header justify-content-between align-items-center row mb-3">
          <h3 class="mb-3 mb-lg-0 col-12 col-md-12 col-lg-3">Мои заявки</h3>
          <div class="broker-block__time col-12 col-lg-9">
            <form action="" class="row align-items-center justify-content-between row"> 
              <div class="col-12 col-sm-2 mb-2 mb-sm-0">Период:</div> 
              <div class="input-group mb-0 col-12 col-sm-4 mb-2 mb-sm-0">
                <input type="text" class="form-control datepicker" id='datetimepicker4'>
                <div class="input-group-append">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
              </div>
              <div class="input-group mb-0 col-12 col-sm-4 mb-2 mb-sm-0">
                <input type="text" class="form-control datepicker" id='datetimepicker4'>
                <div class="input-group-append">
                  <span class="input-group-text d-"><i class="far fa-calendar-alt"></i></span>
                </div>
              </div>
              <div class="col-12 col-sm-2 mb-2 mb-sm-0">
                <button class="btn btn-primary btn-block text-uppercase">ОК</button>
              </div>
            </form>
          </div>
        </div>
        <div class="overflow-auto">
          <table class="table table-bordered text-center">
            <thead class="bg-secondary">
              <tr>
                <th scope="col">Дата</th>
                <th scope="col">Номер</th>
                <th scope="col">ФИО Клиента</th>
                <th scope="col">Город</th>
                <th scope="col">Сумма</th>
                <th scope="col">Статус</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>15.10.18</td>
                <td>47111</td>
                <td>Иванов Иван Иванович</td>
                <td>Тюмень</td>
                <td>300 000</td>
                <td>В работе</td>
              </tr>
              <tr>
                <td>12.10.18</td>
                <td>46789</td>
                <td>Петров Петр Петрович</td>
                <td>Москва</td>
                <td>800 000</td>
                <td>Отказ</td>
              </tr>
              <tr>
                <td>08.10.18</td>
                <td>46679</td>
                <td>Сергеева Наталья Сергеевна</td>
                <td>C.Петербург</td>
                <td>1 500 000</td>
                <td>В работе</td>
              </tr>
              <tr>
                <td>02.10.18</td>
                <td>46560</td>
                <td>Козлова Инна Витальевна</td>
                <td>Курган</td>
                <td>500 000</td>
                <td>Выдано</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="broker-block mb-5 border-top-0">
        <div class="broker-block__header justify-content-between align-items-center row mb-3">
          <h3 class="mb-3 mb-lg-0 col-12 col-md-12 col-lg-3">Выплаты и отчеты</h3>
          <div class="broker-block__time col-12 col-lg-9">
            <form action="" class="row align-items-center justify-content-between row"> 
              <div class="col-12 col-sm-2 mb-2 mb-sm-0">Период:</div> 
              <div class="input-group mb-0 col-12 col-sm-4 mb-2 mb-sm-0">
                <input type="text" class="form-control datepicker" id='datetimepicker4'>
                <div class="input-group-append">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
              </div>
              <div class="input-group mb-0 col-12 col-sm-4 mb-2 mb-sm-0">
                <input type="text" class="form-control datepicker" id='datetimepicker4'>
                <div class="input-group-append">
                  <span class="input-group-text d-"><i class="far fa-calendar-alt"></i></span>
                </div>
              </div>
              <div class="col-12 col-sm-2 mb-2 mb-sm-0">
                <button class="btn btn-primary btn-block text-uppercase">ОК</button>
              </div>
            </form>
          </div>
        </div>
        <div class="overflow-auto">
          <table class="table table-bordered text-center">
            <thead class="bg-secondary">
              <tr>
                <th scope="col">Дата</th>
                <th scope="col">Номер заявки</th>
                <th scope="col">Сумма займа</th>
                <th scope="col">Сумма выплаты</th>
                <th scope="col">Оплата</th>
                <th scope="col">Отчет агента</th>
                <th scope="col">Оригиналы получены</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>15.10.18</td>
                <td>47111</td>
                <td>300 000</td>
                <td>300</td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
                <td class="broker-icon-blue"><i class="fas fa-download fa-2x"></i></td>
                <td class="broker-icon-red"><i class="fas fa-times fa-2x"></i></td>
              </tr>
              <tr>
                <td>12.10.18</td>
                <td>46789</td>
                <td>800 000</td>
                <td>800</td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
                <td class="broker-icon-blue"><i class="fas fa-download fa-2x"></i></td>
                <td class="broker-icon-red"><i class="fas fa-times fa-2x"></i></td>
              </tr>
              <tr>
                <td>08.10.18</td>
                <td>46679</td>
                <td>1 500 000</td>
                <td>1 500</td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
              </tr>
              <tr>
                <td>02.10.18</td>
                <td>46560</td>
                <td>500 000</td>
                <td>500</td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
                <td class="broker-icon-blue"><i class="fas fa-check fa-2x"></i></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="broker-action">
        <h2 class="broker-action__title font-weight-bold">Слайдер для Акции</h2>
        <img class="img-fluid" src="invest/assets/images/broker-action-img.png" alt="">
      </div>
    
    </div>
    <div class="col-lg-4 col-md-12 justify-content-between">

      <div class="wrap h-100 justify-content-between d-flex flex-column">

        <div class="card work-files text-center overflow-hidden">
          <div class="card-body">
            <h3 class="my-4">Рабочие файлы</h3>
            <div class="work-files__list text-left d-flex flex-column mb-2">
              <a href="#"><i class="far fa-file-alt"></i>Инструкция брокера</a>
              <a href="#"><i class="far fa-file-alt"></i>Паспорт продукта</a>
              <a href="#"><i class="far fa-file-alt"></i>Оферта</a>
            </div>
          </div>
          <div class="work-files__contacts py-4 d-flex flex-column justify-content-between">
            <h3 class="mb-3">Каналы для брокеров</h3>
            <div>
              <a class="mr-4" href="#"><i class="fab fa-vk fa-2x"></i></a>
              <a class="mr-4" href="#"><i class="fab fa-telegram-plane fa-2x"></i></a>
              <a href="#"><i class="fas fa-bullhorn fa-2x"></i></a>
            </div>
          </div>
        </div>

        <div class="card manager-card border-0 mb-4 text-center">
          <div class="card-body position-relative px-0">
            <h3 class="text-center manager-title px-3">Персональный менеджер:</h3>
            <div class="manager-info d-flex justify-content-between align-items-center my-4 text-left">
              <img src="/invest/assets/images/menedger1.png" alt="" class="manager-info__photo"> 
              <div class="manager-info__name">
                Ирина Подгорнова
                <div class="manager-post mt-1">Менеджер по работе с инвесторами</div> 
              </div>
            </div>
            <div class="manager-buttons d-flex justify-content-between mb-4">
              <a href="#" class="btn btn-primary"><i class="fas fa-comment"></i>Онлайн-чат</a>
              <a href="#" class="btn btn-outline-primary manager-btn-phone">
              <svg version="1.1" width="15" height="15" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 480.56 480.56" fill="#5a93ad" xml:space="preserve">
                  <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                    c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                    c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                    c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                    c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                    c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                  <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                    c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                  <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                    l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
              </svg>
                Позвонить</a>
            </div>
            <div class="card-line"></div>
          </div>
        </div>

        <div class="card manager-card border-0 mb-4 text-center">
          <div class="card-body position-relative px-0">
            <h3 class="text-center manager-title px-3">Куратор брокеров:</h3>
            <div class="manager-info d-flex justify-content-between align-items-center my-4 text-left">
              <img src="/invest/assets/images/menedger1.png" alt="" class="manager-info__photo"> 
              <div class="manager-info__name">
                Ирина Подгорнова
                <div class="manager-post mt-1">Менеджер по работе с инвесторами</div> 
              </div>
            </div>
            <div class="manager-buttons d-flex justify-content-between mb-4">
              <a href="#" class="btn btn-primary"><i class="fas fa-comment"></i>Онлайн-чат</a>
              <a href="#" class="btn btn-outline-primary manager-btn-phone">
              <svg version="1.1" width="15" height="15" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 480.56 480.56" fill="#5a93ad" xml:space="preserve">
                  <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                    c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                    c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                    c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                    c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                    c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                  <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                    c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                  <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                    l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
              </svg>
                Позвонить</a>
            </div>
            <div class="card-line"></div>
          </div>
        </div>

        <div class="card py-4 mb-4 mb-lg-0 text-center bg-primary text-white">
          <div class="card-body">
            <div class="support">
              <h3 class="support-name">
                Техподдержка
              </h3>
              <div class="support-text">По техническим вопросам работы личного кабинета</div>
              <a href="#" class="btn btn-outline-light support-btn"><i class="fas fa-exclamation-circle"></i>Отправить запрос</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<? include 'invest/footer.php'?>