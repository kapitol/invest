<? include 'invest/header.php'?>
<div class="container">
  <div class="row mb-4">
    <div class="col-md-6 col-lg-4">
      <div class="card h-100 border-0">
        <div class="card-body p-0">
          <div class="contract">
            <h2 class="mb-4 d-flex align-items-center">Договор ПЛС №1 
              <a class="contract-copy d-flex align-items-center ml-2" href="#">
                <svg class="mr-1" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  width="18px" height="23px" fill="#5a93ad"  xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 368 368" xml:space="preserve">
                  <path d="M333.864,82.56h-0.008c0-0.048-0.016-0.024-0.016-0.024h-0.008c-0.056-0.064-0.12-0.128-0.176-0.184l-80-80
                    C252.16,0.848,250.128,0,248,0h-15.64c-0.232,0-0.488,0-0.72,0H56C42.76,0,32,10.768,32,24v320c0,13.232,10.768,24,24,24h256
                    c13.232,0,24-10.768,24-24V88C336,85.896,335.184,83.984,333.864,82.56z M320,344c0,4.408-3.584,8-8,8H55.992
                    c-4.408,0-8-3.592-8-8V24c0-4.408,3.592-8,8-8h168v32H88c-4.416,0-8,3.584-8,8v48c0,4.416,3.584,8,8,8s8-3.584,8-8V64h128v40
                    c0,4.416,3.576,8,8,8h88V344z M320,96h-80V16h4.688L320,91.32V96z"/>
                  <path d="M226.344,242.352L192,276.688V136c0-4.416-3.584-8-8-8c-4.416,0-8,3.584-8,8v140.688l-34.344-34.344
                    c-3.128-3.128-8.184-3.128-11.312,0c-1.56,1.56-2.344,3.608-2.344,5.656c0,2.048,0.784,4.096,2.344,5.656l48,48
                    c1.424,1.424,3.256,2.2,5.12,2.328h0.008C183.648,304,183.824,304,184,304c0.328,0,0.656-0.016,0.976-0.056
                    c0.008,0,0.008,0,0.016,0c1.536-0.192,3.024-0.832,4.264-1.904c0.152-0.136,0.296-0.272,0.44-0.416l47.96-47.96
                    c3.128-3.128,3.128-8.184,0-11.312C234.528,239.224,229.472,239.224,226.344,242.352z"/>
                </svg>
                Скан
              </a>
            </h2>
            <h3 class="mb-3">Сумма займа: 500 000 руб.</h3>
            <ul class="contract-list pl-0">
              <li>Ставка: <span>12,25 %</span></li>
              <li>Срок: <span>12 мес.</span></li>
              <li>Дата начала: <span>10.03.2018</span></li>
              <li>Дата завершения: <span>10.03.2019</span></li>
            </ul>
            <a href="#" class="contract-btn-onlain btn btn-lg btn-primary btn-block">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" xml:space="preserve" width="15px" height="15px">
                <path d="M50.688,48.222C55.232,43.101,58,36.369,58,29c0-7.667-2.996-14.643-7.872-19.834c0,0,0-0.001,0-0.001  c-0.004-0.006-0.01-0.008-0.013-0.013c-5.079-5.399-12.195-8.855-20.11-9.126l-0.001-0.001L29.439,0.01C29.293,0.005,29.147,0,29,0  s-0.293,0.005-0.439,0.01l-0.563,0.015l-0.001,0.001c-7.915,0.271-15.031,3.727-20.11,9.126c-0.004,0.005-0.01,0.007-0.013,0.013  c0,0,0,0.001-0.001,0.002C2.996,14.357,0,21.333,0,29c0,7.369,2.768,14.101,7.312,19.222c0.006,0.009,0.006,0.019,0.013,0.028  c0.018,0.025,0.044,0.037,0.063,0.06c5.106,5.708,12.432,9.385,20.608,9.665l0.001,0.001l0.563,0.015C28.707,57.995,28.853,58,29,58  s0.293-0.005,0.439-0.01l0.563-0.015l0.001-0.001c8.185-0.281,15.519-3.965,20.625-9.685c0.013-0.017,0.034-0.022,0.046-0.04  C50.682,48.241,50.682,48.231,50.688,48.222z M2.025,30h12.003c0.113,4.239,0.941,8.358,2.415,12.217  c-2.844,1.029-5.563,2.409-8.111,4.131C4.585,41.891,2.253,36.21,2.025,30z M8.878,11.023c2.488,1.618,5.137,2.914,7.9,3.882  C15.086,19.012,14.15,23.44,14.028,28H2.025C2.264,21.493,4.812,15.568,8.878,11.023z M55.975,28H43.972  c-0.122-4.56-1.058-8.988-2.75-13.095c2.763-0.968,5.412-2.264,7.9-3.882C53.188,15.568,55.736,21.493,55.975,28z M28,14.963  c-2.891-0.082-5.729-0.513-8.471-1.283C21.556,9.522,24.418,5.769,28,2.644V14.963z M28,16.963V28H16.028  c0.123-4.348,1.035-8.565,2.666-12.475C21.7,16.396,24.821,16.878,28,16.963z M30,16.963c3.179-0.085,6.3-0.566,9.307-1.438  c1.631,3.91,2.543,8.127,2.666,12.475H30V16.963z M30,14.963V2.644c3.582,3.125,6.444,6.878,8.471,11.036  C35.729,14.45,32.891,14.881,30,14.963z M40.409,13.072c-1.921-4.025-4.587-7.692-7.888-10.835  c5.856,0.766,11.125,3.414,15.183,7.318C45.4,11.017,42.956,12.193,40.409,13.072z M17.591,13.072  c-2.547-0.879-4.991-2.055-7.294-3.517c4.057-3.904,9.327-6.552,15.183-7.318C22.178,5.38,19.512,9.047,17.591,13.072z M16.028,30  H28v10.038c-3.307,0.088-6.547,0.604-9.661,1.541C16.932,37.924,16.141,34.019,16.028,30z M28,42.038v13.318  c-3.834-3.345-6.84-7.409-8.884-11.917C21.983,42.594,24.961,42.124,28,42.038z M30,55.356V42.038  c3.039,0.085,6.017,0.556,8.884,1.4C36.84,47.947,33.834,52.011,30,55.356z M30,40.038V30h11.972  c-0.113,4.019-0.904,7.924-2.312,11.58C36.547,40.642,33.307,40.126,30,40.038z M43.972,30h12.003  c-0.228,6.21-2.559,11.891-6.307,16.348c-2.548-1.722-5.267-3.102-8.111-4.131C43.032,38.358,43.859,34.239,43.972,30z   M9.691,47.846c2.366-1.572,4.885-2.836,7.517-3.781c1.945,4.36,4.737,8.333,8.271,11.698C19.328,54.958,13.823,52.078,9.691,47.846  z M32.521,55.763c3.534-3.364,6.326-7.337,8.271-11.698c2.632,0.945,5.15,2.209,7.517,3.781  C44.177,52.078,38.672,54.958,32.521,55.763z" fill="#FFFFFF"/>
              </svg>
              Пополнить Онлайн
            </a>
            <button type="button" class="contract-btn btn btn-lg  btn-outline-primary btn-block" data-toggle="modal" data-target="#cashbackModal"><i class="fas fa-times fa-1x"></i>Досрочное востребование</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="card d-flex flex-column justify-content-between h-100 mt-4 mt-md-0 border-md-0 total-payment-card">
        <div class="card-body d-none d-md-block total-payment text-center bg-secondary ">
          <h3 class="mt-2 mb-5">Всего выплачено</h3>
          <img class="img-fluid" src="/invest/assets/images/total-payment-graph.png" alt="" class="total-payment__img">
        </div>
        <div class="total-payment__summ text-center py-3 bg-blue">Общая сумма:  31 084,32</div>
      </div>
    </div>
    <div class="col-lg-4 d-none d-lg-block">
      <div class="card border-0 h-100 bg-secondary">
        <div class="card-body personal d-flex flex-column justify-content-between m-auto">
          <div>
            <h3 class="text-center mb-4 mt-2">
              Иванов Аркадий Геннадьевич
            </h3>
            <div class="personal-data">
              <a href="tel:+79123456789" class="personal-data__item d-block">
                <span>Телефон:</span> +7 (912) 345-67-89
              </a>
              <a href="mailto:ivanov.ag@yandex.ru" class="personal-data__item d-block">
                <span>E-mail:</span> ivanov.ag@yandex.ru
              </a>
            </div>
          </div>
          <div class="card-line"></div>
          <div>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-user"></i>Профиль</button>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-pencil-alt"></i>редактировать</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8 col-md-12">
      <div class="card overflow-auto">
        <div class="card-body overflow-y">
          <div class="account">
            <div class="account-top d-flex justify-content-between mb-4">
              <h3 class="account-top__name">
                Расчеты по договору
              </h3>
              <a href="/" class="account-top__order"><i class="far fa-file-alt mr-2"></i>Заказать Справку 2-НДФЛ</a>
            </div>
            <table class="account-table table table-bordered text-center">
              <thead class="bg-secondary">
                <tr>
                  <th scope="col">Дата</th>
                  <th scope="col">Дата платежа</th>
                  <th scope="col">Начислено</th>
                  <th scope="col">Выплачено</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>01.10.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.09.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.08.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.07.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.06.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.05.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.04.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
                <tr>
                  <td>01.03.18</td>
                  <td>Начислено по договору ПЛС №1</td>
                  <td>5 104</td>
                  <td>4 440,62</td>
                </tr>
              </tbody>
              <tfoot>
                <tr class="bg-secondary">
                  <td></td>
                  <td></td>
                  <td>Итого:</td>
                  <td>31 084,32</td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>  
    </div>
    <div class="col-lg-4 col-md-12 justify-content-between">
    <div class="wrap h-100 justify-content-between d-flex flex-column">
      <div class="card manager-card border-0 mb-4 text-center">
          <div class="card-body position-relative px-0">
            <h3 class="text-center manager-title px-3">Ваш менеджер:</h3>
            <div class="manager-info d-flex justify-content-between align-items-center my-4 text-left">
              <img src="/invest/assets/images/menedger1.png" alt="" class="manager-info__photo"> 
              <div class="manager-info__name">
                Ирина Подгорнова
                <div class="manager-post mt-1">Менеджер по работе с инвесторами</div> 
              </div>
            </div>
            <div class="manager-buttons d-flex justify-content-between mb-4">
              <a href="#" class="btn btn-primary"><i class="fas fa-comment"></i>Онлайн-чат</a>
              <a href="#" class="btn btn-outline-primary manager-btn-phone">
              <svg version="1.1" width="15" height="15" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 480.56 480.56" fill="#5a93ad" xml:space="preserve">
                  <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                    c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                    c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                    c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                    c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                    c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                  <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                    c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                  <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                    l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
              </svg>
                Позвонить</a>
            </div>
            <div class="card-line"></div>
          </div>
        </div>

        <div class="card py-4 mb-4 mb-lg-0 text-center bg-primary text-white">
          <div class="card-body">
            <div class="support">
              <h3 class="support-name">
                Техподдержка
              </h3>
              <div class="support-text">По техническим вопросам работы личного кабинета</div>
              <a href="#" class="btn btn-outline-light support-btn"><i class="fas fa-exclamation-circle"></i>Отправить запрос</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="economy d-none d-lg-block">
    <h2 class="economy-title text-center mb-4 font-weight-semibold">Актуальные программы сбережений</h2>
    <h2 class="economy-subtitle text-center mb-4 font-weight-normal">Калькулятор доходности</h2>
    <div class="economy-summ">
      <div class="row">
        <div class="col-4 m-auto">
          <div class="form-group mb-5">
            <label for="" class="economy-label text-muted font-weight-light text-center w-100">Выберите сумму в рублях</label>
            <input class="economy-summ__result font-weight-semibold form-control text-center mb-3" type="text" value="100000">  
            <input type="range" class="custom-range  mb-5" min="100000" max="1000000" step="0.5" value="100000" id="customRange3">
            <label for="" class="economy-label text-muted text-center font-weight-light d-block">Выберите программу сбережений</label>
          </div>
        </div>
      </div>
      <div class="row mb-5">
        <div class="col-4 mb-5">
          <div class="card">
            <div class="card-body text-center">
              <h3 class="economy-title mb-3 font-weight-light">«Экспресс 9,5%»</h3>
              <div class="economy-rate mb-3">
                <span class="font-weight-light text-muted economy-label">годовых</span>
                <div class="economy-rate__result font-weight-semibold bg-secondary w-75 mx-auto mt-1">100 000</div>
              </div>
              <div class="economy-profit">
                <span class="font-weight-light text-muted economy-label">Ежемесячный доход</span>
                <div class="economy-profit__result mb-1 mt-2 font-weight-semibold">792</div>
                <span class="font-weight-light text-muted economy-label">рублей</span>
              </div>
            </div>
            <a href="#" class="economy-btn btn-block btn btn-lg btn-primary text-uppercase">оставить заявку</a>  
          </div>
        </div>
        <div class="col-4 mb-5">
          <div class="card">
            <div class="card-body text-center">
              <h3 class="economy-title mb-3 font-weight-light">«Стабильность 11%»</h3>
              <div class="economy-rate mb-3">
                <span class="font-weight-light text-muted economy-label">годовых</span>
                <div class="economy-rate__result font-weight-semibold bg-secondary w-75 mx-auto mt-1">300 000</div>
              </div>
              <div class="economy-profit">
                <span class="font-weight-light text-muted economy-label">Ежемесячный доход</span>
                <div class="economy-profit__result mb-1 mt-2 font-weight-semibold">2 750</div>
                <span class="font-weight-light text-muted economy-label">рублей</span>
              </div>
            </div>
            <a href="#" class="economy-btn btn-block btn btn-lg btn-primary text-uppercase">оставить заявку</a>  
          </div>
        </div>
        <div class="col-4 mb-5">
          <div class="card">
            <div class="card-body text-center">
              <h3 class="economy-title mb-3 font-weight-light">«Стабильность 13%»</h3>
              <div class="economy-rate mb-3">
                <span class="font-weight-light economy-label text-muted">годовых</span>
                <div class="economy-rate__result font-weight-semibold bg-secondary w-75 mx-auto mt-1">500 000</div>
              </div>
              <div class="economy-profit">
                <span class="font-weight-light text-muted economy-label">Ежемесячный доход</span>
                <div class="economy-profit__result mb-1 mt-2 font-weight-semibold">5 833</div>
                <span class="font-weight-light text-muted economy-label">рублей</span>
              </div>
            </div>
            <a href="#" class="economy-btn btn-block btn btn-lg btn-primary text-uppercase">оставить заявку</a>  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<? include 'invest/footer.php'?>