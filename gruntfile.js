module.exports = function (grunt) {
	const bitrixSiteTemplate = 'invest';
    const sass = require('node-sass');
    grunt.initConfig({
        notify: {
            watch: {
                options: {
                    title: 'Task Complete',
                    message: 'SASS and Uglify finished running',
                    duration: 1,
                }
            },
        },
        uglify: {
            files: { 
                src: [ 'invest/**/*.js', '!invest/**/*.min.js', '!invest/**/*.map.js'],
                dest: '',
                expand: true,
                ext: '.min.js',
                extDot: 'last'
            }
        },
        cssmin: {
            files: { 
                src: [ 'invest/**/*.css', '!invest/**/*.min.css', '!invest/**/*.map.css'],
                dest: '',
                expand: true,
                ext: '.min.css',
                extDot: 'last'
            }
        },
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: ['last 2 versions']
                    })
                ]
            },
            dist: {
                src: `${bitrixSiteTemplate}/template_styles.css`
            }
        },
    	sass: {
			options: {
				implementation: sass,
				sourceMap: true
			},
            dist: {
                files: [
                  {
                    src: [`${bitrixSiteTemplate}/assets/scss/main.scss`],
                    dest: `${bitrixSiteTemplate}/template_styles.css`,
                  },
                  {
                    cwd: `${bitrixSiteTemplate}/assets/scss/pages/`,
                    src: ['*.scss'],
                    dest: `${bitrixSiteTemplate}/assets/css/pages/`,
                    ext: '.css',
                    extDot: 'first',
                    expand: true,
                  },
                ]
              }
        
		},
        watch: {
            css: { files: [`${bitrixSiteTemplate}/assets/scss/**/*.scss`], tasks: [ 'scss', 'notify:watch' ] }
        },
    });

    // load plugins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-notify');

    // register at least this one task
    grunt.registerTask('default', [ 'watch' ]);
    grunt.registerTask('scss', [ 'sass', 'postcss' ]);
    grunt.registerTask('prod', [ 'scss', 'uglify', 'cssmin' ]);
};