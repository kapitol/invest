<? include 'invest/header.php'?>
<div class="container">
  <div class="row mb-4">
    <div class="col-lg-8 col-md-12">
      <div class="card overflow-auto">
        <div class="card-body overflow-y">
          <div class="account">
            <div class="account-top mb-4">
              <h3 class="account-top__name">
                Расчеты по договору
              </h3>
              <div class="account-top__subtext">Выберите один из Ваших договоров</div>
            </div>
            <table class="account-table table table-bordered text-center">
              <thead class="bg-secondary">
                <tr>
                  <th scope="col">Договор</th>
                  <th scope="col">Дата начала</th>
                  <th scope="col">Дата завершения</th>
                  <th scope="col">Сумма (руб)</th>
                  <th scope="col">Статус</th>
                  <th scope="col">Скан</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="contract_detail.php">ПЛС №1</a></td>
                  <td>10.03.18</td>
                  <td>500 000</td>
                  <td>-</td>
                  <td>Активен</td>
                  <td><a href="#"><img class="account-img" src="invest/assets/images/file.svg" alt=""></a></td>
                </tr>
                <tr class="account-close">
                  <td>ПЛС №2</td>
                  <td>01.02.18</td>
                  <td>300 000</td>
                  <td>01.02.2018</td>
                  <td>Закрыт</td>
                  <td><a href="#"></a></td>
                </tr>
                <tr class="account-close">
                  <td>ПЛС №3</td>
                  <td>01.08.16</td>
                  <td>400 000</td>
                  <td>01.01.17</td>
                  <td>Закрыт</td>
                  <td><a href="#"></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>  
    </div>
    <div class="col-lg-4 d-none d-lg-block">
      <div class="card border-0 h-100 bg-secondary">
        <div class="card-body personal d-flex flex-column justify-content-between m-auto">
          <div>
            <h3 class="text-center mb-4 mt-2">
              Иванов Аркадий Геннадьевич
            </h3>
            <div class="personal-data">
              <a href="tel:+79123456789" class="personal-data__item d-block">
                <span>Телефон:</span> +7 (912) 345-67-89
              </a>
              <a href="mailto:ivanov.ag@yandex.ru" class="personal-data__item d-block">
                <span>E-mail:</span> ivanov.ag@yandex.ru
              </a>
            </div>
          </div>
          <div class="card-line"></div>
          <div>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-user"></i>Профиль</button>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-pencil-alt"></i>редактировать</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-4">
    <div class="col-lg-8">
      <div class="card py-4 mb-4 mb-lg-0 text-center bg-primary text-white">
        <div class="card-body">
          <div class="support">
            <h3 class="support-name">
              Техподдержка
            </h3>
            <div class="support-text">По техническим вопросам работы личного кабинета</div>
            <a href="#" class="btn btn-outline-light support-btn"><i class="fas fa-exclamation-circle"></i>Отправить запрос</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-12">
      <div class="card manager-card h-100 border-0 text-center">
        <div class="card-body position-relative px-0 py-0 d-flex flex-column justify-content-between">
          <h3 class="text-center px-3">Ваш менеджер:</h3>
          <div class="manager-info d-flex justify-content-between align-items-center text-left">
            <img src="/invest/assets/images/menedger1.png" alt="" class="manager-info__photo"> 
            <div class="manager-info__name font-weight-light">
              Диана Дворянчикова
              <div class="manager-post mt-1">Менеджер по работе с инвесторами</div> 
            </div>
          </div>
          <div class="manager-buttons d-flex justify-content-between">
            <a href="#" class="btn btn-primary"><i class="fas fa-comment"></i>Онлайн-чат</a>
            <a href="#" class="btn btn-outline-primary manager-btn-phone">
            <svg version="1.1" width="15" height="15" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              viewBox="0 0 480.56 480.56" fill="#5a93ad" xml:space="preserve">
                <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                  c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                  c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                  c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                  c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                  c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                  c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                  l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
            </svg>
              Позвонить</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="economy d-none d-lg-block">
    <h2 class="economy-title text-center mb-4 font-weight-semibold">Актуальные программы сбережений</h2>
    <h2 class="economy-subtitle text-center mb-4 font-weight-normal">Калькулятор доходности</h2>
    <div class="economy-summ">
      <div class="row">
        <div class="col-4 m-auto">
          <div class="form-group mb-5">
            <label for="" class="economy-label text-muted font-weight-light text-center w-100">Выберите сумму в рублях</label>
            <input class="economy-summ__result font-weight-semibold form-control text-center mb-3" type="text" value="100000">  
            <input type="range" class="custom-range  mb-5" min="100000" max="1000000" step="1" id="customRange3" value="100000">
            <label for="" class="economy-label text-muted text-center font-weight-light d-block">Выберите программу сбережений</label>
          </div>
        </div>
      </div>
      <div class="row mb-5">
        <div class="col-4 mb-5">
          <div class="card">
            <div class="card-body text-center">
              <h3 class="economy-title mb-3 font-weight-light">«Экспресс 9,5%»</h3>
              <div class="economy-rate mb-3">
                <span class="font-weight-light text-muted economy-label">годовых</span>
                <div class="economy-rate__result font-weight-semibold bg-secondary w-75 mx-auto mt-1">100 000</div>
              </div>
              <div class="economy-profit">
                <span class="font-weight-light text-muted economy-label">Ежемесячный доход</span>
                <div class="economy-profit__result mb-1 mt-2 font-weight-semibold">792</div>
                <span class="font-weight-light text-muted economy-label">рублей</span>
              </div>
            </div>
            <a href="#" class="economy-btn btn-block btn btn-lg btn-primary text-uppercase">оставить заявку</a>  
          </div>
        </div>
        <div class="col-4 mb-5">
          <div class="card">
            <div class="card-body text-center">
              <h3 class="economy-title mb-3 font-weight-light">«Стабильность 11%»</h3>
              <div class="economy-rate mb-3">
                <span class="font-weight-light text-muted economy-label">годовых</span>
                <div class="economy-rate__result font-weight-semibold bg-secondary w-75 mx-auto mt-1">300 000</div>
              </div>
              <div class="economy-profit">
                <span class="font-weight-light text-muted economy-label">Ежемесячный доход</span>
                <div class="economy-profit__result mb-1 mt-2 font-weight-semibold">2 750</div>
                <span class="font-weight-light text-muted economy-label">рублей</span>
              </div>
            </div>
            <a href="#" class="economy-btn btn-block btn btn-lg btn-primary text-uppercase">оставить заявку</a>  
          </div>
        </div>
        <div class="col-4 mb-5">
          <div class="card">
            <div class="card-body text-center">
              <h3 class="economy-title mb-3 font-weight-light">«Стабильность 13%»</h3>
              <div class="economy-rate mb-3">
                <span class="font-weight-light economy-label text-muted">годовых</span>
                <div class="economy-rate__result font-weight-semibold bg-secondary w-75 mx-auto mt-1">500 000</div>
              </div>
              <div class="economy-profit">
                <span class="font-weight-light text-muted economy-label">Ежемесячный доход</span>
                <div class="economy-profit__result mb-1 mt-2 font-weight-semibold">5 833</div>
                <span class="font-weight-light text-muted economy-label">рублей</span>
              </div>
            </div>
            <a href="#" class="economy-btn btn-block btn btn-lg btn-primary text-uppercase">оставить заявку</a>  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<? include 'invest/footer.php'?>