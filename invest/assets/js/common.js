$(function () {
  $('.graph-payment-card').click(function () {
    $('.graph-payment-card').each(function () {
      $(this).toggleClass('graph-payment-card-active');
    });
  });
  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.offcanvas-collapse').toggleClass('open')
  });
  $('.custom-file-input').change(function(e){
    var filename = $(this).val().replace(/.+[\\\/]/, "");
    $(this).siblings('.custom-file-label').html(filename);
  });
  $('.datepicker').datetimepicker({
    locale: 'ru',
    format: 'L'
  });
});