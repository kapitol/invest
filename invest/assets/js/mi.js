/*!
 * MI JavaScript Library v0.0.2
 * https://dmi.agency/
 *
 * Date: 2018-09-28
 */

var MI = {
    // CORE
    config: {
        notifications: true
    },
    vars: {

    },
    init: function () {
        // <a href="#">Anchor</a>
        $("a[href*='\\#']").on('click', function (e) {
            e.preventDefault();
        });

        // Bootstrap JavaScript Components
        if (typeof $.fn.popover === 'function') {
            $('[data-toggle="popover"]').popover();
        }
        if (typeof $.fn.tooltip === 'function') {
            $('[data-toggle="tooltip"]').tooltip();
        }
    },
    // -------------------------------------------------------------------------

    // AJAX
    ajax: function (data, func, params) {
        var params = params || {};

        data.ajax = 1;

        // Основные параметры
        var settings = {
            method: params.method || "GET",
            url: params.url || "",
            async: params.async || true,
            data: data
        };

        $.ajax(settings)
                .done(function (msg) {
                    // Пришел ответ с сервера
                    var data = '';

                    try {
                        data = JSON.parse(msg);
                    } catch (err) {
                        data = msg;
                    }

                    if (typeof data === 'object') {
                        if (data.error != undefined) {
                            MI.core.banner({
                                text: data.error.msg,
                                type: 'error'
                            });
                            return false;
                        }

                        if (func !== undefined) {
                            func(data);
                            delete data;
                            delete msg;
                        }
                    } else {
                        alert('Error! Response is String.'); // @todo Заменить на MI.core.banner()
                    }
                })
                .fail(function (data) {
                    // Ошибка
                    console.log("fail"); // @todo Заменить на MI.core.banner()
                    console.log(data); // @todo Заменить на MI.core.banner()
                })
                .always(function () {
                    // Выполняется всегда
                    console.log("always"); // @todo Заменить на MI.core.banner()
                });

        return false;
    },

    // Banner / Alert
    banner: function (opts) {
        var opts = opts || {};

        opts.text = opts.text || 'Notification';

        if (MI.config.notifications === true) {
            opts.theme = 'bootstrap-v4';
            opts.type = opts.type || 'alert';

            new Noty(opts).show();
        } else {
            alert(opts.text);
        }
    },

    // Array
    // ---------------------------------------------------------------------
    // use: MI.utils.inArray(1, [1, 2, 3]);
    inArray: function (what, where) {
        for (var i = 0; i < Object.keys(where).length; i++) {
            if (what === where[i]) {
                return true;
            }
        }

        return false;
    },

    // Colors
    // ---------------------------------------------------------------------
    // use: MI.utils.getHexRGBColor('rgb(0,0,0)');
    getHexRGBColor: function (color) {
        color = color.replace(/\s/g, "");
        var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);

        if (aRGB) {
            color = '';
            for (var i = 1; i <= 3; i++) {
                color += Math.round((aRGB[i][aRGB[i].length - 1] == "%" ? 2.55 : 1) * parseInt(aRGB[i])).toString(16).replace(/^(.)$/, '0$1')
            }
        } else {
            color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3')
        }

        return color;
    },

    // Cookie
    // ---------------------------------------------------------------------
    setCookie: function (name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            expires = "; expires=" + date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000)).toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    getCookie: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    },

    // Number
    // ---------------------------------------------------------------------
    // Числовые склонения
    // use: MI.utils.declOfNum(count, ['яблоко', 'яблока', 'яблок']);
    // hint: [1 яблоко, 2-4 яблока, 5.. яблок]
    declOfNum: function (count, titles) {
        if (titles.length === 3) {
            return titles[(count % 10 === 1 && count % 100 !== 11) ? 0 : count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20) ? 1 : 2];
        }

        return false;
    },

    // String
    // ---------------------------------------------------------------------
    // Очистить строку от лишних символов
    cleanString: function (str) {
        return str ? str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;') : '';
    },

    // "+" to "%2B"
    replaceSpecialChars: function (str) {
        return str.split('+').join('%2B');
    },

    // Clear whitespace, убрать лишние пробелы
    trim: function (str) {
        return (str || '').replace(/^\s+|\s+$/g, '');
    },

    // Первая буква в верхний регистр
    ucfirst: function (str) {
        return str ? str.slice(0, 1).toUpperCase() + str.slice(1) : '';
    }
};

$(document).ready(function () {
    MI.init();
});