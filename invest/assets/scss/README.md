# SCSS
Данный SCSS основан на Bootstrap с добавлением улучшений.

## Структура

- **bootstrap** - Основные компоненты Bootstrap (ядро)
- **mi** - Компоненты Media Instance
- **custom** - Расширенные компоненты Bootstrap
- **template** - Подключение всех файлов *.scss для компилирования

```
scss/
├── bootstrap/
│   ├── required/
│   ├── layout/
│   ├── content/
│   ├── components/
│   └── utilities/
├── mi/
│   ├── required/
│   ├── layout/
│   ├── content/
│   ├── components/
│   ├── utilities/
│   └── template/
├── custom/
│   ├── required/
│   ├── layout/
│   ├── content/
│   ├── components/
│   ├── utilities/
│   └── template/
└── template/
    └── templateName.scss
```

Логика нашего SCSS строится по следующему принципу: В папке **scss/bootstrap/** хранится ядро Bootstrap (т.е. исходные файлы).

Папку **assets/scss/bootstrap/** править **НЕЛЬЗЯ!**

## Пользовательские настройки
- **_bootstrap.scss** - Параметры Bootstrap
- **_mi.scss** - Параметры Media Instance
- **_custom.scss** - Пользовательские параметры

```
scss/
└── custom/
    └── required/
        └── variables/
            ├── _bootstrap.scss
            ├── _mi.scss
            └── _custom.scss
```

## Принцип разработки
Рассмотрим пример. Дизайнер подготовил вам макет. Вы посмотрели макет и хотите разместить кнопку на странице сайта.

Компонент **Button** есть в Bootstrap, по этому копируем код кнопки (с сайта Bootstrap) и размещаем на странице.

```html
<button type="button" class="btn btn-primary">Primary</button>
```

Далее необходимо стилизовать кнопку в соответствии с макетом.

Вам понадобятся следующие файлы:

- **scss/bootstrap/components/_buttons.scss** - только для просмотра
- **scss/custom/required/variables/_bootstrap.scss** - переменные для настройки кнопки

Возможны 2 случая: свойство CSS есть в компоненте Bootstrap или этого свойства нет.

### CSS свойство есть в компоненте
Если свойство есть в компоненте, то его можно изменить через переменные.
Например, свойство **padding**. У копмонента **Button** padding меняется через mixin (это можно увидеть в файле: scss/bootstrap/components/_buttons.scss).

Видим, что у класса **.btn** есть переменные **$btn-padding-y** и **$btn-padding-x**. Их мы и будем менять.

Открываем файл **scss/custom/required/variables/_bootstrap.scss**, ищем эти переменные, меняем их значения и ставим вконце **!global**.

```scss
/* scss/bootstrap/components/_buttons.scss */
.btn {
  @include button-size($btn-padding-y, $btn-padding-x, $btn-font-size, $btn-line-height, $btn-border-radius);
}

/* scss/custom/required/variables/custom.scss */
$input-btn-padding-y: 2rem !global;
$btn-padding-y: $input-btn-padding-y !default;
/* или */
$btn-padding-y: 2rem !global;
```
**ВАЖНО!** !global дописывать только у той переменной, которую мы изменяем (видно из примера выше).

### CSS свойства нет в компоненте

----
OLD
----

- Вы смотрите компонент в папке Bootstrap _(scss/bootstrap/components/buttons.scss)_
- Находите нужное CSS свойство
- Изменяете его в файле переменных _(scss/custom/required/variables/bootstrap.scss)_

Если же свойства нет, то необходимо "расширить" компонент:
- Создаете пустой файл компонента _(scss/custom/components/buttons.scss)_
- Добавляете класс и свойство
- Выносите значение свойства в переменную _(scss/custom/required/variables/custom.scss)_

```scss
/* scss/custom/required/variables/custom.scss */
$btn-text-transform: uppercase !global;
```

```scss
/* scss/bootstrap/components/buttons.scss - исходный стиль*/
.btn {
  display: inline-block;
  font-weight: $btn-font-weight;
  text-align: center;
  ...
}


/* scss/custom/components/buttons.scss - расширенный стиль */
.btn {
  text-transform: $btn-text-transform;
}
```

#### H4
##### H5
###### H6
