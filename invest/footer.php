
<footer class="footer bg-secondary">
  <div class="container">
    <div class="row py-5">
      <div class="col-lg-4 col-md-6 col-sm-12 d-flex flex-column justify-content-between mb-3">
        <div class="footer-menu pb-2">
          <ul class="list-unstyled text-uppercase">
            <li><a href="#">О нас</a></li>
            <li><a href="#">брокерам</a></li>
            <li><a href="#">программы сбережений</a></li>
          </ul>
        </div>
        <div class="footer-copyright d-none d-md-block">
          2018 @ КПК “Капитоль Кредит”
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-12 mb-3">
        <div class="footer-documents">
          <ul class="list-unstyled m-0">
            <li><a href="#">Свидетельство КПК</a></li>
            <li><a href="#">Правила финансовой взаимопомощи</a></li>
            <li><a href="#">Свидетельство о членстве в СРО КПК</a></li>
            <li><a href="#">Публичная оферта для партнеров</a></li>
            <li><a href="#">Политика конфиденциальности “Капитль Кредит”</a></li>
            <li><a href="#">Положение КПК о выдаче займов</a></li>
            <li><a href="#">Защита персональных данных</a></li>
            <li><a href="#">Реквизиты компании</a></li>
            <li><a href="#">Общие условия договора ипотечного займа</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 mb-sm-3 mb-0">
        <div class="footer-contacts text-center text-md-right d-flex flex-column">
          <a href="tel:+78005505421" class="footer-contacts__phone font-weight-bold pb-4 mb-1 d-block">8 800 550 54 21</a>
          <div class="footer-contacts__address order-3 order-md-2  mb-4">
            ОГРН 1177232007468
            Адрес г. Тюмень, ул. Герцена, 64, оф.903
          </div>
          <div class="footer-social order-2 order-md-3  mb-3 d-flex justify-content-center justify-content-md-end order-2">
            <a class="mr-3" href="#"><i class="fab fa-vk fa-2x"></i></a>
            <a class="mr-3" href="#"><i class="fab fa-facebook-f fa-2x"></i></a>
            <a class="mr-0" href="#"><i class="fab fa-odnoklassniki fa-2x"></i></a>
          </div>
          <div class="footer-copyright d-md-none d-lg-none d-xl-none d-sm-block order-4">
            2018 @ КПК “Капитоль Кредит”
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<div class="cashback">
  <div class="modal fade" id="cashbackModal" tabindex="-1" role="dialog" aria-labelledby="cashbackModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content text-center">
        <div class="modal-header">
          <h3 class="modal-title" id="cashbackModal">Заявка на востребование денежных средств</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body py-0">
          <p class="p-0">Таким образом дальнейшее развитие различных форм деятельности играет важную роль в формировании форм развития. Повседневная практика показывает, что начало повседневной работы по формированию позиции в значительной степени.</p>
        </div>
        <div class="modal-footer p-0 justify-content-center">
          <div class="container-fluid">
            <div class="row justify-content-center">
              <div class="col-md-5 col-sm-8 col-12">
                <form class="application mb-5">
                  <div class="form-group mx-auto p-2 bg-secondary">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="applicationCash" id="partialCash" value="option1" checked>
                      <label class="form-check-label application-label font-weight-normal" for="partialCash">Частичное</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="applicationCash" id="fullCash" value="option2">
                      <label class="form-check-label application-label font-weight-normal" for="fullCash">Полное</label>
                    </div>
                  </div>
                  <div class="form-group mx-auto">
                    <label for="applicationSumm">Сумма:</label>
                    <input type="text" class="form-control" id="applicationSumm" value="3 000 000">
                  </div>
                  <button type="submit" class="btn btn-primary btn-block text-uppercase">отправить</button>
                </form> 
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="cancel-application">
  <div class="modal fade" id="cancelApplication" tabindex="-1" role="dialog" aria-labelledby="canselApplication" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content text-center">
        <div class="modal-header">
          <button type="button" class="close pb-0" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <h3 class="modal-title px-2" id="cancelApplication">Вы уверены что хотите отменить заявку?</h3>
        <div class="modal-body pb-0">
          <p class="p-0">В дальнейшем вы сможете создать новую заявку в личном кабинете</p>
        </div>
        <div class="modal-footer justify-content-center">
          <div class="container-fluid">
            <div class="row justify-content-center mb-4">
              <div class="col-md-4 col-sm-8 col-12 mb-3">
                <button class="btn btn-primary text-uppercase btn-lg btn-block cancel-application rounded-0" data-dismiss="modal">Да</button>
              </div>
              <div class="col-md-4 col-sm-8 col-12 mb-3">
                <button class="btn btn-primary text-uppercase btn-lg btn-block cancel-application rounded-0" data-dismiss="modal">нет</button>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="loan-application">
  <div class="modal fade" id="loanAplication" tabindex="-1" role="dialog" aria-labelledby="loanAplication" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content text-center">
        <div class="modal-header">
          <button type="button" class="close pb-0" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <h3 class="modal-title" id="loanAplication">Заявка на займ</h3>
        <div class="modal-body loan-application__content pb-0">
          <nav class="loan-steps pb-3 mb-4">
            <div class="nav nav-tabs loan-steps__list d-flex justify-content-between flex-nowrap" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active loan-steps__item font-weight-bold" href="#loan-step-1" data-toggle="tab">Шаг 1<span class="d-none d-sm-block font-weight-normal">Основная <br> информация</span></a>
              <a class="nav-item nav-link loan-steps__item font-weight-bold" href="#loan-step-2" data-toggle="tab">
                <svg class="step-arrow" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
                  <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                    C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                    c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                </svg>
                Шаг 2<span class="d-none d-sm-block font-weight-normal">Объект залога</span></a> 
              <a class="nav-item nav-link loan-steps__item font-weight-bold" href="#loan-step-3" data-toggle="tab">
                <svg class="step-arrow" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
                  <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                    C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                    c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                </svg>
                Шаг 3<span class="d-none d-sm-block font-weight-normal">Паспортные <br> данные</span></a>
            </div>
          </nav> 
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="loan-step-1" role="tabpanel" aria-labelledby="nav-home-tab">
              <form action="" class="form-inline">
                <div class="row w-100 mx-auto">
                  <div class="col-12">
                    <h3 class="text-left mb-3">1. Информация о заемщике</h3>
                  </div>
                </div> 
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="secondNameLoan">Фамилия:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="secondNameLoan" name="second_name_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="nameLoan">Имя:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="nameLoan" name="name_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="patronymicNameLoan">Отчество:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="patronymicNameLoan" name="patronymic_name_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="phoneIdLoan">Телефон:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="phoneIdloan" name="phone_loan">           
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="emailIdLoan">E-mail:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="emailIdLoan" name="email_loan">
                  </div>
                </div>
                <div class="row w-100 mx-auto">
                  <div class="col-12">
                    <h3 class="text-left mb-3 mt-4">2. Информация о займе</h3>
                  </div>
                </div>  
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="summId">Сумма:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="summId" name="summ">                 
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="targetId">Цель займа:</label>
                  <div class="col-12 col-sm-8">
                    <select class="form-control w-100" type="text" id="targetId" name="target">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-3">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="timeId">Срок:</label>
                  <div class="col-12 col-sm-8">
                    <select class="form-control w-100" type="text" id="timeId" name="time">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                </div>
                <div class="row w-100 mt-3 mb-5">
                  <div class="col-sm-5 col-12 mx-auto">
                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase mb-2">Далее</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane fade" id="loan-step-2" role="tabpanel" aria-labelledby="nav-home-tab">
              <form action="" class="form-inline">
                <div class="row w-100 mx-auto">
                  <div class="col-12">
                    <h3 class="text-left mb-3">3. Объект залога:</h3>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="objectId">Тип объекта:</label>
                  <div class="col-12 col-sm-8">
                    <select class="form-control w-100" type="text" id="objectId" name="object">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="cityLoan">Город:</label>
                  <div class="col-12 col-sm-8">
                    <select class="form-control w-100" type="text" id="cityLoan" name="city_loan">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="streetId">Улица:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="streetId" name="street">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="nunmerHouse">Дом:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="nunmerHouse" name="house">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="apartamentId">Квартира:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="apartamentId" name="apartament">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="ownerId">Собственники:</label>
                  <div class="col-12 col-sm-8">
                    <select class="form-control w-100" type="text" id="ownerId" name="owner">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <div class="col-5 col-sm-6">
                    <div class="row">
                      <label class="col-12 col-sm-8 col-form-label text-left" for="floorId">Этаж:</label>
                      <div class="col-12 col-sm-4">
                        <input class="form-control w-100" type="text" id="floorId" name="floor">           
                      </div>
                    </div>
                  </div>
                  <div class="col-7 col-sm-6">
                    <div class="row">
                      <label class="col-12 col-sm-8 col-form-label text-left" for="totalFloors">Общая этажность:</label>
                      <div class="col-12 col-sm-4">
                        <input class="form-control w-100" type="text" id="totalFloors" name="total_floors">           
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-start w-100 mb-2">
                  <label class="col-12 col-sm-4 pr-0 col-form-label text-left" for="yearConstruction">Год постройки дома:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="yearConstruction" name="year_construction">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between align-items-center w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="apartamentId">Док-т-основание:</label>
                  <div class="col-12 col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="document">
                      <label class="custom-file-label" for="document"></label>
                    </div>
                  </div>
                </div>
                <div class="row w-100 mx-auto">
                  <div class="col-12">
                    <h3 class="text-left mb-3 mt-4">4. Я подтверждаю что:</h3>
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <div class="col-12 text-left">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="consentDeal">
                      <label class="custom-control-label" for="consentDeal">Все собственники согласны на сделку</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="consentChildren">
                      <label class="custom-control-label" for="consentChildren">Нет несовершеннолетних собственников</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="consentObject">
                      <label class="custom-control-label" for="consentObject">Объект не находится в аресте/залоге</label>
                    </div>
                  </div> 
                </div>

                <div class="row w-100 mt-3 mb-5">
                  <div class="col-sm-5 col-12 mx-auto">
                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase mb-2">Далее</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane fade" id="loan-step-3" role="tabpanel" aria-labelledby="nav-contact-tab">
              <form action="" class="form-inline">
                <h3 class="text-left mb-3">3. Паспортные данные:</h3>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportRangeLoan">Серия:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportRangeLoan" name="passport_range_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportNumberLoan">Номер:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportNumberLoan" name="passport_umber_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportIssuedLoan">Выдан:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportIssuedLoan" name="passport_issued_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportDateLoan">Дата выдачи:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportDateLoan" name="passport_date_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label pr-0" for="passportCodeLoan">Код подразделения:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportCodeLoan" name="passport_code_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportDateBirthLoan">Дата рождения:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportDateBirthLoan" name="passport_date_birth_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportBirthPlaceLoan">Место рождения:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportBirthPlaceLoan" name="passport_birth_place_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label pr-0" for="passportAddressloan">Адрес регистрации:</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportAddressloan" name="passport_address_loan">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <label class="col-12 col-sm-4 col-form-label text-left" for="passportFamilyStatus">Сем. положение::</label>
                  <div class="col-12 col-sm-8">
                    <input class="form-control w-100" type="text" id="passportFamilyStatus" name="passport_family_status">
                  </div>
                </div>
                <div class="form-group mx-auto row justify-content-between w-100 mb-2">
                  <div class="col-12">
                    <div class="custom-control custom-checkbox confirm-form text-left">
                      <input type="checkbox" class="custom-control-input" id="consentConfirm">
                      <label class="custom-control-label confirm-form__text" for="consentConfirm">Нажимая кнопку "Отправить" я даю согласие на обработку персональных данных</label>
                    </div>
                  </div> 
                </div>
                <div class="row w-100 mt-3 mb-5">
                  <div class="col-sm-5 col-12 mx-auto">
                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase mb-2">отправить</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="personal-card">
  <div class="modal fade" id="personalCard" tabindex="-1" role="dialog" aria-labelledby="personalCard" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content text-center">
        <div class="modal-header">
          <button type="button" class="close pb-0" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="row w-100">
          <div class="col-12 mx-auto text-center">
            <h1 class="mb-3 font-weight-bold">Иванов Аркадий Геннадьевич</h1>
            <p>Статус: заемщик (подтвержден)</p>
          </div>
        </div> 
        <div class="modal-body loan-application__content pb-0">
        <form action="" class="form-inline">
          <div class="row w-100 mx-auto">
            <div class="col-12">
              <h3 class="text-left mb-3">1. Подробная информация о залоге</h3>
            </div>
          </div> 
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="phoneIdPersonal">Телефон:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="phoneIdPersonal" name="phone_personal">           
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="emailIdPersonal">E-mail:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="emailIdPersonal" name="email_personal">
            </div>
          </div>
          <div class="row w-100 mx-auto">
            <div class="col-12">
              <h3 class="text-left mb-3 mt-4">2. Паспортные данные</h3>
            </div>
          </div>  
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passportRangePersonal">Серия:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportRangePersonal" name="passport_range_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passportNumberPersonal">Номер:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportNumberPersonal" name="passport_umber_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passportIssuedPersnal">Выдан:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportIssuedPersonal" name="passport_issued_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passportDatePersonal">Дата выдачи:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportDatePersonal" name="passport_date_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label pr-0 text-left" for="passportCodePersonal">Код подразделения:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportCodePersonal" name="passport_code_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passportDateBirthPersonal">Дата рождения:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportDateBirthPersonal" name="passport_date_birth_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passportBirthPlacePersonal">Место рождения:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportBirthPlacePersonal" name="passport_birth_place_personal">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left pr-0" for="passportAddressPersonal">Адрес регистрации:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="passportAddressPersonal" name="passport_address_personal">
            </div>
          </div>
          <div class="row w-100 mx-auto">
            <div class="col-12">
              <h3 class="text-left mb-3 mt-4">3. Данные счета</h3>
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label pr-0 text-left" for="checkAccount">Расч./ счет:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="checkAccount" name="check_account">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label pr-0 text-left" for="bankAccount">БИК Банка:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="bankAccount" name="bank_account">
            </div>
          </div>
          <div class="row w-100 mt-3 mb-5">
            <div class="col-sm-5 col-12 mx-auto">
              <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase mb-2">Сохранить</button>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="broker-ending">
  <div class="modal fade" id="brokerEnding" tabindex="-1" role="dialog" aria-labelledby="brokerEnding" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content text-center">
        <div class="modal-header">
          <button type="button" class="close pb-0" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="row w-100">
          <div class="col-12 mx-auto text-center">
            <h1 class="mb-3 font-weight-bold">Добавление сотрудника</h1>
          </div>
        </div> 
        <div class="modal-body loan-application__content pb-0">
        <form action="" class="form-inline">
          <div class="row w-100 mx-auto">
            <div class="col-12">
              <h3 class="text-left mb-3">1. Информация о сотруднике</h3>
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="secondNameEnding">Фамилия:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="secondNameEnding" name="second_name_ending">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="namenameEnding">Имя:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="nameEnding" name="name_ending">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="patronymicNameEnding">Отчество:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="patronymicNameEnding" name="patronymic_name_ending">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="phoneIdEnding">Телефон:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="phoneIdEnding" name="phone-ending">           
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="emailIdEnding">E-mail:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="emailIdEnding" name="email_ending">
            </div>
          </div>
          <div class="row w-100 mx-auto">
            <div class="col-12">
              <h3 class="text-left mb-3 mt-4">2. Доступ к ЛК</h3>
            </div>
          </div>  
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="right">Права:</label>
            <div class="col-12 col-sm-8">
              <select class="form-control w-100" type="text" id="right" name="target">
                <option>1</option>
                <option>2</option>
                <option>3</option>
              </select>
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="login">Логин:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="text" id="login" name="login">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passwordId">Пароль:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="password" id="passwordId" name="password">
            </div>
          </div>
          <div class="form-group mx-auto row justify-content-between w-100 mb-2">
            <label class="col-12 col-sm-4 col-form-label text-left" for="passwordConfirm">Подтверждение:</label>
            <div class="col-12 col-sm-8">
              <input class="form-control w-100" type="password" id="passwordConfirm" name="password_confirm">
            </div>
          </div>
          <div class="row w-100 mt-5 mb-5">
            <div class="col-sm-6 col-12 mx-auto mb-3">
              <button type="button" class="btn btn-outline-primary btn-lg btn-block text-uppercase mb-2">отмена</button>
            </div>
            <div class="col-sm-6 col-12 mx-auto">
              <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase mb-2">Сохранить</button>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/invest/assets/plugins/jquery/jquery.min.js"></script><!-- 3.3.1 -->
<script src="/invest/assets/plugins/bootstrap/bootstrap.min.js"></script><!-- 4.2 -->
<script src="/invest/assets/plugins/fontawesome/all.min.js"></script><!-- 5.5.0 -->
<script src="/invest/assets/plugins/moment/moment.js"></script>
<script src="/invest/assets/plugins/moment/moment-with-locales.js"></script>
<script src="/invest/assets/plugins/datepicker/bootstrap-datetimepicker.js"></script>
<script src="/invest/assets/js/common.js"></script><!-- 5.5.0 -->

</body>
</html>