
<!DOCTYPE html>
<html lang="ru">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Капитоль кредит</title>
  <link rel="stylesheet" href="invest/assets/plugins/datepicker/bootstrap-datetimepicker-build.css">    
  <link rel="stylesheet" href="invest/template_styles.css">
</head>
<body>
  <header class="header mt-3 mb-4 d-none d-lg-block">
    <div class="container">
      <div class="row align-items-center">
        <div class="col">
          <div class="header-logo mb-2">
            <a href="/" class="header-logo__img d-block"><img class="w-100" src="/invest/assets/images/logo.png" alt=""></a>
          </div>
          <div class="header-logo__text">
            Сильный союзник!
          </div>
        </div>
        <div class="col">
          <div class="header-city__form">
            <form action="">
              <div class="form-group mb-0">
                  <label class="mb-0" for="cityMain">Ваш город:</label>
                  <select class="form-control" id="cityMain">
                    <option>Тюмень</option>
                    <option>Москва</option>
                  </select>
              </div>
            </form>
            <div class="header-city__address">
              ул.50 лет ВЛКСМ 49, ст.3, офис 302
            </div>
          </div>
        </div>
        <div class="col">
          <div class="header-personal text-center">
            <a href="#" class="header-personal__btn btn btn-outline-primary">
              <span><i class="fas fa-user"></i></span>Личный кабинет
            </a>  
          </div>
        </div>
        <div class="col">
          <div class="header-contacts text-right">
            <a href="tel:+78002221726" class="header-contacts__phone">8 (800) 222-17-26</a>
            <div class="header-personal__graph">
              Ежедневно с 8:00 до 17:00 МСК Звонок бесплатный
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container-fluid sticky-top bg-white">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <a class="navbar-brand d-lg-none" href="#"><img class="w-100" src="/invest/assets/images/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="offcanvas">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse offcanvas-collapse text-center" id="navbarsExampleDefault">
          <div class="card border-0 bg-secondary d-lg-none my-3">
            <div class="card-body personal d-flex flex-column justify-content-between m-auto">
              <div>
                <h3 class="text-center mb-4 mt-2">
                  Иванов Аркадий Геннадьевич
                </h3>
                <div class="personal-data mb-3">
                  <a href="tel:+79123456789" class="personal-data__item d-block">
                    <span>Телефон:</span> +7 (912) 345-67-89
                  </a>
                  <a href="mailto:ivanov.ag@yandex.ru" class="personal-data__item d-block">
                    <span>E-mail:</span> ivanov.ag@yandex.ru
                  </a>
                </div>
              </div>
              <div class="card-line"></div>
              <div>
                <a href="#" class="btn btn-primary btn-block btn-lg text-uppercase"><svg class="svg-inline--fa fa-user fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path></svg><!-- <i class="fas fa-user"></i> -->Профиль</a>
                <a href="#" class="btn btn-primary btn-block btn-lg text-uppercase"><svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <i class="fas fa-pencil-alt"></i> -->редактировать</a> 
              </div>
            </div>
          </div>
          <ul class="navbar-nav justify-content-lg-between flex-fill">
            <li class="nav-item dropdown" >
              <a href="#" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">Получить заем</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="request.php">Моя заявка на займ</a>
                <a class="dropdown-item" href="main_loans.php">Мои займы</a>
              </div>
            </li>
            <li class="nav-item"><a href="#" class="nav-link">Программы сбережений</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Партнерам</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Способы оплаты</a></li>
            <li class="nav-item"><a href="#" class="nav-link">О нас</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Блог</a></li>
          </ul>
          <div class="header-contacts d-lg-none my-4">
            <a href="tel:+78002221726" class="header-contacts__phone">8 (800) 222-17-26</a>
            <div class="header-personal__graph">
              Ежедневно с 8:00 до 17:00 МСК Звонок бесплатный
            </div>
          </div>
          <div class="header-city__form d-lg-none my-4">
            <form action="">
              <div class="form-group mb-0">
                  <label class="mb-0" for="cityMobile">Ваш город:</label>
                  <select class="form-control" id="cityMobile">
                    <option>Тюмень</option>
                    <option>Москва</option>
                  </select>
              </div>
            </form>
            <div class="header-city__address">
              ул.50 лет ВЛКСМ 49, ст.3, офис 302
            </div>
          </div>
          <div class="header-personal d-lg-none">
            <a href="#" class="header-personal__btn btn btn-outline-primary">
            <span><svg class="svg-inline--fa fa-user fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path></svg><!-- <i class="fas fa-user"></i> --></span>Личный кабинет
            </a>  
          </div>
        </div> 
      </nav>
    </div>
  </div>


