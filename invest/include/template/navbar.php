<?php defined('B_PROLOG_INCLUDED') || die; ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
    <div class="container">
        <a class="navbar-brand" href="/">

            <img src="<?= SITE_TEMPLATE_PATH; ?>/assets/brand/logo dmi.svg" height="30"
                 class="d-inline-block text-white" alt="MI Bitrix">
            Bitrix
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
            <span class="navbar-toggler-icon"></span>
        </button>


        <?php // Меню ?>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/catalog/">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/news/">Новости</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Контакты</a>
                </li>
            </ul>

            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                        Дополнения
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item"
                           href="/gui/">GUI</a>
                        <a class="dropdown-item"
                           href="http://www.1c-bitrix.ru/download/scripts/restore.php"
                           target="_blank">Restore.php</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/bitrix/" target="_blank">Администрирование</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
