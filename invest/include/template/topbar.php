<?php defined('B_PROLOG_INCLUDED') || die; ?>

<div class="topbar bg-dark text-white-50">
    <div class="container">
        <div class="row justify-content-between small">

            <?php // Контакты ?>
            <div class="col-xl-4 col-lg-4 col-md-4 col-4">
                <div class="row h-100 justify-content-start">
                    <div class="col-xl-10 col-lg-12 col-auto">
                        <div class="row h-100 justify-content-between">

                            <?php // E-mail ?>
                            <div class="col-auto d-none d-lg-block">
                                <div class="h-100 py-2">
                                    <a href="mailto:<?= file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/include_area/company/email.php"); ?>"
                                       class="text-white-50">
                                        <?php
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:main.include", "", [
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH"           => "/include_area/company/email.php",
                                            "EDIT_TEMPLATE"  => "",
                                        ], false
                                        );
                                        ?>
                                    </a>
                                </div>
                            </div>


                            <?php // Телефон ?>
                            <div class="col-sm-auto col-12">
                                <div class="h-100 py-2">
                                    <a href="tel:<?= preg_replace('/[^0-9+]/', '', file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/include_area/company/phone.php")); ?>"
                                       class="text-white-50">
                                        <?php
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:main.include", "", [
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH"           => "/include_area/company/phone.php",
                                            "EDIT_TEMPLATE"  => "",
                                        ], false
                                        );
                                        ?>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <?php // Город ?>
            <div class="col-xl-4 col-lg-4 col-md-4 col-4">
                <div class="row h-100">
                    <div class="col-12">
                        <div class="row h-100 justify-content-center">

                            <?php // Город ?>
                            <div class="col-auto">
                                <div class="py-2">
                                    Тюмень
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <?php // Вход ?>
            <div class="col-xl-4 col-lg-4 col-md-4 col-4">
                <div class="row h-100 justify-content-end">
                    <div class="col-xl-6 col-lg-8 col-auto">
                        <div class="row h-100 justify-content-between">

                            <?php // Вход ?>
                            <div class="col-sm-auto col-12">
                                <div class="h-100 py-2">
                                    <a href="/auth/" class="text-white-50">Вход</a>
                                </div>
                            </div>


                            <?php // Регистрация ?>
                            <div class="col-auto d-none d-lg-block">
                                <div class="h-100 py-2">
                                    <a href="/auth/?register=yes" class="text-white-50">Регистрация</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>