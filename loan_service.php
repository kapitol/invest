<? include 'invest/header.php'?>
<div class="container">
  <div class="row mb-4">
    <div class="col-md-6 col-lg-4">
      <div class="card h-100 border-0">
        <div class="card-body p-0">
          <div class="contract">
            <h2 class="mb-4 d-flex align-items-center">Договор ДИЗ №111 
              <a class="contract-copy d-flex align-items-center ml-2" href="#">
                <svg class="mr-1" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  width="18px" height="23px" fill="#5a93ad"  xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 368 368" xml:space="preserve">
                  <path d="M333.864,82.56h-0.008c0-0.048-0.016-0.024-0.016-0.024h-0.008c-0.056-0.064-0.12-0.128-0.176-0.184l-80-80
                    C252.16,0.848,250.128,0,248,0h-15.64c-0.232,0-0.488,0-0.72,0H56C42.76,0,32,10.768,32,24v320c0,13.232,10.768,24,24,24h256
                    c13.232,0,24-10.768,24-24V88C336,85.896,335.184,83.984,333.864,82.56z M320,344c0,4.408-3.584,8-8,8H55.992
                    c-4.408,0-8-3.592-8-8V24c0-4.408,3.592-8,8-8h168v32H88c-4.416,0-8,3.584-8,8v48c0,4.416,3.584,8,8,8s8-3.584,8-8V64h128v40
                    c0,4.416,3.576,8,8,8h88V344z M320,96h-80V16h4.688L320,91.32V96z"/>
                  <path d="M226.344,242.352L192,276.688V136c0-4.416-3.584-8-8-8c-4.416,0-8,3.584-8,8v140.688l-34.344-34.344
                    c-3.128-3.128-8.184-3.128-11.312,0c-1.56,1.56-2.344,3.608-2.344,5.656c0,2.048,0.784,4.096,2.344,5.656l48,48
                    c1.424,1.424,3.256,2.2,5.12,2.328h0.008C183.648,304,183.824,304,184,304c0.328,0,0.656-0.016,0.976-0.056
                    c0.008,0,0.008,0,0.016,0c1.536-0.192,3.024-0.832,4.264-1.904c0.152-0.136,0.296-0.272,0.44-0.416l47.96-47.96
                    c3.128-3.128,3.128-8.184,0-11.312C234.528,239.224,229.472,239.224,226.344,242.352z"/>
                </svg>
                Скан
              </a>
            </h2>
            <h3 class="mb-3">Сумма займа: 500 000 руб.</h3>
            <ul class="contract-list pl-0">
              <li>Ставка: <span>12,25 %</span></li>
              <li>Срок: <span>12 мес.</span></li>
              <li>Дата начала: <span>10.03.2018</span></li>
              <li>Дата завершения: <span>10.03.2019</span></li>
            </ul>
            <a href="#" class="contract-btn-onlain btn btn-lg btn-primary btn-block">
              <i class="fas fa-pencil-alt"></i>
              Внести платеж
            </a>
            <button type="button" class="contract-btn btn btn-lg  btn-outline-primary btn-block" data-toggle="modal" data-target="#cashbackModal"><i class="fas fa-check"></i>Досрочное погашение</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="card issue-payment text-center border-0 overflow-hidden mt-4 mt-md-0 ">
        <h3 class="mb-2 bg-secondary issue-payment__title">
          Когда и сколько платить?
        </h3>
        <div class="d-flex justify-content-between mb-2">
          <div class="issue-payment__summ mr-1 bg-secondary">Дата следующего списания<span class="font-weight-bold mt-2">10.11.2018</span></div>
          <div class="issue-payment__summ ml-1 bg-secondary">Сумма следующего платежа<span class="font-weight-bold mt-2">8 840 руб.</span></div>
        </div>
        <div class="issue-payment__warning font-weight-semibold">
          Внимание! Платеж необходимо внести за 3-4 дня до даты списания
        </div> 
      </div>  
    </div>
    <div class="col-lg-4 d-none d-lg-block">
      <div class="card border-0 h-100 bg-secondary">
        <div class="card-body personal d-flex flex-column justify-content-between m-auto">
          <div>
            <h3 class="text-center mb-4 mt-2">
              Иванов Аркадий Геннадьевич
            </h3>
            <div class="personal-data">
              <a href="tel:+79123456789" class="personal-data__item d-block">
                <span>Телефон:</span> +7 (912) 345-67-89
              </a>
              <a href="mailto:ivanov.ag@yandex.ru" class="personal-data__item d-block">
                <span>E-mail:</span> ivanov.ag@yandex.ru
              </a>
            </div>
          </div>
          <div class="card-line"></div>
          <div>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-user"></i>Профиль</button>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-pencil-alt"></i>редактировать</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-8 col-md-12">
      <div class="card overflow-auto border-info loan-card">
        <div class="card-body overflow-y">
          <h3 class="my-4">Расчеты по договору</h3>
          <table class="table table-bordered text-center mb-4">
            <thead class="bg-secondary">
              <tr>
                <th scope="col">Номер платежа</th>
                <th scope="col">Дата платежа</th>
                <th scope="col">Размер платежа</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>10.04.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>2</td>
                <td>10.05.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>3</td>
                <td>10.06.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>4</td>
                <td>10.07.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>5</td>
                <td>10.08.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>6</td>
                <td>10.09.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>7</td>
                <td>10.10.18</td>
                <td>8 840 руб.</td>
              </tr>
              <tr>
                <td>8</td>
                <td>10.11.18</td>
                <td>8 840 руб.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>  
    </div>
    <div class="col-lg-4 col-md-12 justify-content-between">
    <div class="wrap h-100 justify-content-between d-flex flex-column">
      <div class="card manager-card border-0 mb-4 text-center">
          <div class="card-body position-relative px-0">
            <h3 class="text-center manager-title px-3">Ваш менеджер:</h3>
            <div class="manager-info d-flex justify-content-between align-items-center my-4 text-left">
              <img src="/invest/assets/images/menedger1.png" alt="" class="manager-info__photo"> 
              <div class="manager-info__name">
                Ирина Подгорнова
                <div class="manager-post mt-1">Менеджер по работе с инвесторами</div> 
              </div>
            </div>
            <div class="manager-buttons d-flex justify-content-between mb-4">
              <a href="#" class="btn btn-primary"><i class="fas fa-comment"></i>Онлайн-чат</a>
              <a href="#" class="btn btn-outline-primary manager-btn-phone">
              <svg version="1.1" width="15" height="15" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 480.56 480.56" fill="#5a93ad" xml:space="preserve">
                  <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                    c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                    c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                    c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                    c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                    c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                  <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                    c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                  <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                    l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
              </svg>
                Позвонить</a>
            </div>
            <div class="card-line"></div>
          </div>
        </div>

        <div class="card py-4 mb-4 mb-lg-0 text-center bg-primary text-white">
          <div class="card-body">
            <div class="support">
              <h3 class="support-name">
                Техподдержка
              </h3>
              <div class="support-text">По техническим вопросам работы личного кабинета</div>
              <a href="#" class="btn btn-outline-light support-btn"><i class="fas fa-exclamation-circle"></i>Отправить запрос</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<? include 'invest/footer.php'?>