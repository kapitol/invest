<? include 'invest/header.php'?>
<div class="container">
  <div class="row">
    <div class="col-lg-8 justify-content-between flex-column">
      <h2 class="mb-4">Моя заявка на займ</h2>
      <div class="card loan-card border-info mb-4">
        <div class="card-body text-center">
          <div class="row my-4">
            <div class="col-12 col-md-11 m-auto">
              <p class="card-text font-weight-bold mb-5">
                У Вас еще нет ни одной поданной заявки на займ. <br>
                Вы можете отправить ее воспользовавшись формой подачи заявки
              </p>
              <button class="btn btn-primary text-uppercase btn-lg rounded-0" data-toggle="modal" data-target="#loanAplication">отправить заявку</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 d-none d-lg-block">
      <div class="card border-0 h-100 bg-secondary">
        <div class="card-body personal d-flex flex-column justify-content-between m-auto">
          <div>
            <h3 class="text-center mb-4 mt-2">
              Иванов Аркадий Геннадьевич
            </h3>
            <div class="personal-data">
              <a href="tel:+79123456789" class="personal-data__item d-block">
                <span>Телефон:</span> +7 (912) 345-67-89
              </a>
              <a href="mailto:ivanov.ag@yandex.ru" class="personal-data__item d-block">
                <span>E-mail:</span> ivanov.ag@yandex.ru
              </a>
            </div>
          </div>
          <div class="card-line"></div>
          <div>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-user"></i>Профиль</button>
            <button data-toggle="modal" data-target="#personalCard" class="btn btn-primary btn-block btn-lg text-uppercase"><i class="fas fa-pencil-alt"></i>редактировать</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-8 col-md-12 mb-3">
      <div class="card rounded-0">
        <div class="card-body">
          <div class="row my-2 my-md-5">
            <div class="col-12 col-md-9 m-auto">
              <h2 class="card-title text-center mb-5 font-weight-normal">
                Калькулятор займа
              </h2>
              <div class="calculator-block mb-5">
                <div class="calculation-top d-flex justify-content-between align-items-center">
                  <div class="calculation-top__text">Сумма займа:</div>
                  <div class="calculation-top__total">300 000</div>
                </div>
                <input type="range" class="custom-range calculation-summ" min="300000" max="5000000" step="1" id="calcSumm" value="300000">
                <div class="calculation-bottom mt-2 d-flex justify-content-between">
                  <div class="calculation-bottom__item">от 300 000 руб.</div>
                  <div class="calculation-bottom__item">до 5 000 000 руб.</div>
                </div>
              </div>
              <div class="calculator-block mb-5">
                <div class="calculation-top d-flex justify-content-between align-items-center">
                  <div class="calculation-top__text">Срок займа:</div>
                  <div class="calculation-top__total">10 лет</div>
                </div>
                <input type="range" class="custom-range calcTime" min="1" max="10" step="3" id="calcTime" value="1">
                <div class="calculation-bottom d-flex justify-content-between mt-2">
                  <div class="calculation-bottom__item">1</div>
                  <div class="calculation-bottom__item">3</div>
                  <div class="calculation-bottom__item">5</div>
                  <div class="calculation-bottom__item">10</div>
                </div>
              </div>
              <h3 class="card-title font-weight-normal text-center mb-4">Ежемесячный платеж:</h3>
              <div class="calculator-block">
                <div class="calculation-top d-flex justify-content-between align-items-center">
                  <div class="calculation-top__text">«Стабильный»</div>
                  <div class="calculation-top__total">12 952 руб.</div>
                </div>
                <div class="calculation-top d-flex justify-content-between align-items-center">
                  <div class="calculation-top__text">«Легкий старт»</div>
                  <div class="calculation-top__total">9 552 руб.</div>
                </div>
                <button class="btn btn-primary btn-lg text-uppercase btn-block mt-3 rounded-0">Получить консультацию</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-12 justify-content-between">
      <div class="card manager-card border-0 mb-4 text-center">
        <div class="card-body position-relative px-0">
          <h3 class="text-center manager-title px-3">Ваш менеджер:</h3>
          <div class="manager-info d-flex justify-content-start align-items-center my-4 text-left">
            <img src="/invest/assets/images/menedger1.png" alt="" class="manager-info__photo"> 
            <div class="manager-info__name">
              Ирина Подгорнова
              <div class="manager-post mt-1">Оператор</div> 
            </div>
          </div>
          <div class="manager-buttons d-flex justify-content-between mb-4">
            <a href="#" class="btn btn-primary"><i class="fas fa-comment"></i>Онлайн-чат</a>
            <a href="#" class="btn btn-outline-primary manager-btn-phone">
            <svg version="1.1" width="15" height="15" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              viewBox="0 0 480.56 480.56" fill="#5a93ad" xml:space="preserve">
                <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                  c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                  c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                  c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                  c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                  c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                  c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                  l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
            </svg>
              Позвонить</a>
          </div>
          <div class="card-line"></div>
        </div>
      </div>
      <div class="card py-3 mb-4 mb-lg-0 text-center bg-primary text-white">
        <div class="card-body">
          <div class="support">
            <h3 class="support-name mb-3">
              Техподдержка
            </h3>
            <a href="#" class="btn btn-outline-light support-btn btn-lg text-uppercase"><i class="fas fa-exclamation-circle"></i>Отправить запрос</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="graph-payment text-center">
  <div class="container">
    <div class="row">
      <div class="col-12 mb-5">
        <h2>График платежей</h2>
        <div>Выберите подходящий Вам график ежемесячного платежа.</div>
      </div>
      <div class="col-12 col-lg-6 col-md-8 mb-5 mx-auto">
        <div class="card graph-payment-card graph-payment-card-active rounded-0">
          <div class="card-body">
            <div class="row mt-3">
              <div class="col-12 col-md-11 col-sm-10 m-auto">
                <h3 class="font-weight-normal">“Стабильный”</h3>
                <p class="card-text">
                  платежи осуществляются равными суммами на протяжении всего периода. 
                </p> 
                <img class="graph-payment__img img-fluid" src="/invest/assets/images/graph-payment1.png" alt="">
                <a href="#" class="btn btn-primary btn-lg btn-block text-uppercase rounded-0 graph-payment__btn">Рассчитать График Платежей</a>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="col-12 col-lg-6 col-md-8 mb-5 mx-auto">
        <div class="card graph-payment-card rounded-0">
          <div class="card-body">
            <div class="row mt-3">
              <div class="col-12 col-md-11 col-sm-10 m-auto">
                <h3 class="font-weight-normal">“Легкий старт”</h3>
                <p class="card-text">
                  платежи осуществляются меньшими суммами в начале и конце периода. 
                </p> 
                <img class="graph-payment__img img-fluid" src="/invest/assets/images/graph-payment2.png" alt="">
                <a href="#" class="btn btn-primary btn-lg btn-block text-uppercase rounded-0 graph-payment__btn">Рассчитать График Платежей</a>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="col-lg-3 col-md-6 col-sm-6 col-11 m-auto">
        <a href="#" class="btn btn-primary btn-lg text-uppercase rounded-0 btn-delta mt-4 btn-block">Получить займ</a>
      </div>
    </div>
  </div>
</div>
<? include 'invest/footer.php'?>